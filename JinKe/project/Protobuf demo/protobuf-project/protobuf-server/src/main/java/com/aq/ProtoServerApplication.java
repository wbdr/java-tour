package com.aq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * proto 服务端
 *
 * @author wu
 */
@SpringBootApplication
public class ProtoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProtoServerApplication.class, args);
    }

}
