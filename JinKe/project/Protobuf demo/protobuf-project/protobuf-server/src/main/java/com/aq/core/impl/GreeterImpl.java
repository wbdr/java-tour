package com.aq.core.impl;

import com.aq.helloword.GreeterGrpc;
import com.aq.helloword.HelloWorldProto;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;


/**
 * GreeterImplBase
 *
 * @author wu
 */
@Slf4j
public class GreeterImpl extends GreeterGrpc.GreeterImplBase {
    @Override
    public void sayHello(HelloWorldProto.HelloRequest request, StreamObserver<HelloWorldProto.HelloReply> responseObserver) {
        String name = request.getName();
        log.info("请求数据信息：" + name);
        HelloWorldProto.HelloReply reply = HelloWorldProto.HelloReply.newBuilder().setMessage(name + " 》》》response over ").build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}
