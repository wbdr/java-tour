package com.aq.core.impl;

import com.aq.onetomany.HelloOneToManyProto;
import com.aq.onetomany.OneToManyStreamingGreeterGrpc;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

/**
 * 服务端流式rpc
 * 一个请求对象，服务端可以传回多个结果对象
 *
 * @author wu
 */
@Slf4j
public class GreeterOneToManyStreamingImpl extends OneToManyStreamingGreeterGrpc.OneToManyStreamingGreeterImplBase {
    @Override
    public void sayHelloOneToManyStreaming(HelloOneToManyProto.HelloRequest request, StreamObserver<HelloOneToManyProto.HelloReply> responseObserver) {
        String name = request.getName();
        String gender = request.getGender();
        request.getGenderBytes();
        log.info("请求信息 --->" + name + "<--->" + gender);

        //返回多个结果
        responseObserver.onNext(HelloOneToManyProto.HelloReply.newBuilder().setMessage("message " + name).setCode("200 gender <-->" + gender).build());
        responseObserver.onNext(HelloOneToManyProto.HelloReply.newBuilder().setMessage("message2 " + name).setCode("200  gender <-->" + gender).build());
        responseObserver.onNext(HelloOneToManyProto.HelloReply.newBuilder().setMessage("message3 " + name).setCode("200 gender <-->" + gender).build());
        //结束返回  事件的最终处理
//        responseObserver.onCompleted();
    }
}
