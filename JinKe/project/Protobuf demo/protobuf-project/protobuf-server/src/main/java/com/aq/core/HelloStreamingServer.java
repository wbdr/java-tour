package com.aq.core;

import com.aq.core.impl.GreeterStreamingImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * grpc服务端
 * 双向流式rpc
 * 结合客户端流式rpc和服务端流式rpc，可以传入多个对象，返回多个响应对象
 *
 * @author wu
 */
@Component
@Slf4j
public class HelloStreamingServer {

    private final int port = 50051;

    private Server server;

    public void init() {
        try {
            this.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建服务
     *
     * @throws Exception
     */
    public void start() throws Exception {
        server = ServerBuilder.forPort(port)
                .addService(new GreeterStreamingImpl())
                .build()
                .start();
        log.info("Server started, listening on  " + port);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Shutting down");
            this.stop();
        }));
    }

    /**
     * 停止服务
     */
    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * 等待主线程的终止，因为grpc库使用守护进程线程。
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

}
