package com.aq.core.impl;

import com.aq.mantoone.HelloManyToOneProto;
import com.aq.mantoone.ManyToOneStreamingGreeterGrpc;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

/**
 * 客户端流式rpc
 * 客户端传入多个请求对象，服务端返回一个响应结果
 *
 * @author wu
 */
@Slf4j
public class GreeterManyToOneStreamingImpl extends ManyToOneStreamingGreeterGrpc.ManyToOneStreamingGreeterImplBase {
    @Override
    public StreamObserver<HelloManyToOneProto.HelloRequest> sayHelloManyToOneStreaming(StreamObserver<HelloManyToOneProto.HelloReply> responseObserver) {

        StreamObserver<HelloManyToOneProto.HelloRequest> streamObserver = new StreamObserver<HelloManyToOneProto.HelloRequest>() {
            //定义返回的builder
            private HelloManyToOneProto.HelloReply.Builder builder = HelloManyToOneProto.HelloReply.newBuilder();

            @Override
            public void onNext(HelloManyToOneProto.HelloRequest helloRequest) {
                String name = helloRequest.getName();
                String gender = helloRequest.getGender();
                log.info("请求数据信息：" + name + "<----->" + gender);
                builder.setMessage(builder.getMessage() + "," + name + "," + gender).setCode("200");
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onCompleted() {
                builder.setMessage(builder.getMessage());
                responseObserver.onNext(builder.build());
                responseObserver.onCompleted();
            }
        };
        return streamObserver;

    }
}
