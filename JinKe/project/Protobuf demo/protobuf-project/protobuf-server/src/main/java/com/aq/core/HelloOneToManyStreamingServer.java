package com.aq.core;

import com.aq.core.impl.GreeterOneToManyStreamingImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * grpc服务端
 * 客户端流式rpc
 * 客户端传入多个请求对象，服务端返回一个响应结果
 *
 * @author wu
 */
@Component
@Slf4j
public class HelloOneToManyStreamingServer {

    private final int port = 50052;

    private Server server;

    public void init() {
        try {
            this.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建服务
     *
     * @throws Exception
     */
    public void start() throws Exception {
        server = ServerBuilder.forPort(port)
                .addService(new GreeterOneToManyStreamingImpl())
                .build()
                .start();
        log.info("Server started, listening on  " + port);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Shutting down");
            this.stop();
        }));
    }

    /**
     * 停止服务
     */
    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * 等待主线程的终止，因为grpc库使用守护进程线程。
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

}
