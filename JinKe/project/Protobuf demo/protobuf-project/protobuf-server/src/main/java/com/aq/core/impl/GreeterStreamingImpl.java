package com.aq.core.impl;

import com.aq.streaming.HelloStreamingProto;
import com.aq.streaming.StreamingGreeterGrpc;
import io.grpc.stub.ServerCallStreamObserver;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicBoolean;


/**
 * 双向流式rpc
 * 结合客户端流式rpc和服务端流式rpc，可以传入多个对象，返回多个响应对象
 * GreeterStreamingImpl
 *
 * @author wu
 */
@Slf4j
public class GreeterStreamingImpl extends StreamingGreeterGrpc.StreamingGreeterImplBase {
    @Override
    public StreamObserver<HelloStreamingProto.HelloRequest> sayHelloStreaming(StreamObserver<HelloStreamingProto.HelloReply> responseObserver) {

        //为请求流设置手动流控制。
        ServerCallStreamObserver<HelloStreamingProto.HelloReply> serverCallStreamObserver =
                (ServerCallStreamObserver<HelloStreamingProto.HelloReply>) responseObserver;
        serverCallStreamObserver.disableAutoInboundFlowControl();

        //防止onNext()和onReady()之间的竞争导致虚假的onReady()调用
        //在执行onNext()时，将isReady()从false切换为true，但是在onNext()检查isReady()之前，
        // request(1)将被调用两次 一次由onNext()调用，一次由onReady()在onNext()调用期间调度
        AtomicBoolean wasReady = new AtomicBoolean(false);

        //为请求流设置一个背压感知消费者。将调用onReadyHandler
        //当消费端有足够的缓冲空间来接收更多的消息时。
        //注意:onReadyHandler的调用与传入的StreamObserver的调用在同一个线程池中序列化
        // onNext()、onError()和onComplete()处理程序。阻塞onReadyHandler将阻止其他消息
        //被来流观察者处理。onReadyHandler必须在一个及时的庄园中返回
        //消息处理
        serverCallStreamObserver.setOnReadyHandler(() -> {
            if (serverCallStreamObserver.isReady() && wasReady.compareAndSet(false, true)) {
                log.info("READY");
                //向请求发送方发送一条消息。当isReady()变为true时，就会发生这种情况
                //接收缓冲区有足够的空闲空间来接收更多的消息。调用request()服务于prime
                //消息泵。
                serverCallStreamObserver.request(1);
            }
        });

        //给gRPC一个StreamObserver，它可以观察和处理传入的请求
        StreamObserver<HelloStreamingProto.HelloRequest> helloRequestStreamObserver = new StreamObserver<HelloStreamingProto.HelloRequest>() {
            @Override
            public void onNext(HelloStreamingProto.HelloRequest request) {
                String name = request.getName();
                log.info("请求参数信息--> " + name);
                String message = "Hello " + name;
                HelloStreamingProto.HelloReply reply = HelloStreamingProto.HelloReply.newBuilder().setMessage(message).build();
                responseObserver.onNext(reply);

                // 检查提供的ServerCallStreamObserver，看看它是否仍然准备接受更多消息。
                if (serverCallStreamObserver.isReady()) {
                    //向发送方发出信号，让它发送另一个请求。只要isReady()保持为真，服务器就会保持
                    //循环遍历onNext() ->请求()…直到客户端
                    //消息耗尽，循环结束，或者服务器耗尽接收缓冲区空间。
                    //如果服务器耗尽了缓冲区空间，isReady()将变为false。当接收缓冲区有
                    //充分消耗，isReady()将变为真，而serverCallStreamObserver的onReadyHandler
                    //将被调用以重新启动消息泵。
                    serverCallStreamObserver.request(1);
                } else {
                    // 如果没有，注意背压已经开始。
                    wasReady.set(false);
                }
            }

            @Override
            public void onError(Throwable throwable) {
                // 如果客户端出现错误，则结束响应流
                throwable.printStackTrace();
                responseObserver.onCompleted();
            }

            @Override
            public void onCompleted() {
                //当客户机结束请求流时，发出工作结束的信号。
                log.info("COMPLETED");
                responseObserver.onCompleted();
            }
        };

        return helloRequestStreamObserver;
    }
}
