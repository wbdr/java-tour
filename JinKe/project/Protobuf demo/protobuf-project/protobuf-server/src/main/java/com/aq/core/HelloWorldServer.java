package com.aq.core;

import com.aq.core.impl.GreeterImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * grpc服务端
 *
 * @author wu
 */
@Component
@Slf4j
public class HelloWorldServer {

    private final int port = 50050;

    private Server server;

    public void init() {
        try {
            this.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建服务
     *
     * @throws Exception
     */
    public void start() throws Exception {
        server = ServerBuilder.forPort(port)
                .addService(new GreeterImpl())
                .build()
                .start();
        log.info("Server started, listening on  " + port);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.err.println("*** shutting down gRPC server since JVM is shutting down");
            this.stop();
            System.err.println("*** server shut down");
        }));

    }

    /**
     * 停止服务
     */
    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * 等待主线程的终止，因为grpc库使用守护进程线程。
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

}
