package com.aq;

import com.aq.core.HelloManyToOneStreamingServer;
import com.aq.core.HelloOneToManyStreamingServer;
import com.aq.core.HelloStreamingServer;
import com.aq.core.HelloWorldServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 创建grpc服务端
 *
 * @author wu
 */
@Component
@Order(1)
public class MyApplicationRunner implements ApplicationRunner {

    @Autowired
    private HelloWorldServer helloWorldServer;

    @Autowired
    private HelloStreamingServer helloStreamingServer;

    @Autowired
    private HelloOneToManyStreamingServer helloOneToManyStreamingServer;

    @Autowired
    private HelloManyToOneStreamingServer helloManyToOneStreamingServer;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        helloWorldServer.init();
        helloStreamingServer.init();
        helloOneToManyStreamingServer.init();
        helloManyToOneStreamingServer.init();
    }
}
