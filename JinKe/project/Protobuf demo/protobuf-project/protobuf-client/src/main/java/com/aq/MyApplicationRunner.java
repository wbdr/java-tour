package com.aq;

import com.aq.core.HelloManyToOneStreamingClient;
import com.aq.core.HelloOneToManyStreamingClient;
import com.aq.core.HelloStreamingClient;
import com.aq.core.HelloWorldClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 创建grpc服务端
 *
 * @author wu
 */
@Component
@Order(1)
public class MyApplicationRunner implements ApplicationRunner {

    @Autowired
    private HelloWorldClient helloWorldClient;

    @Autowired
    private HelloStreamingClient helloStreamingClient;

    @Autowired
    private HelloOneToManyStreamingClient helloOneToManyStreamingClient;

    @Autowired
    private HelloManyToOneStreamingClient helloManyToOneStreamingClient;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
//        helloWorldClient.init("start");
//        helloStreamingClient.init();
//        helloOneToManyStreamingClient.init("","");
//        helloManyToOneStreamingClient.init("张三", "男");
    }
}
