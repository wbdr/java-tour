package com.aq.controller;

import com.alibaba.fastjson.JSONObject;
import com.aq.core.HelloManyToOneStreamingClient;
import com.aq.core.HelloOneToManyStreamingClient;
import com.aq.core.HelloStreamingClient;
import com.aq.core.HelloWorldClient;
import com.aq.helloword.HelloWorldProto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 测试controller
 *
 * @author wu
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @Autowired
    private HelloWorldClient helloWorldClient;

    @Autowired
    private HelloStreamingClient helloStreamingClient;

    @Autowired
    private HelloOneToManyStreamingClient helloOneToManyStreamingClient;

    @Autowired
    private HelloManyToOneStreamingClient helloManyToOneStreamingClient;

    /**
     * 简单rpc
     * 这就是一般的rpc调用，一个请求对象对应一个返回对象
     */
    @RequestMapping(value = "/testOne", method = {RequestMethod.POST, RequestMethod.GET})
    public String testOne(String str) {
        HelloWorldProto.HelloReply reply = helloWorldClient.init(str);
        if (reply != null) {
            return reply.getMessage();
        }
        return "error";
    }


    /**
     * 双向流式rpc
     * 结合客户端流式rpc和服务端流式rpc，可以传入多个对象，返回多个响应对象
     */
    @RequestMapping(value = "/testTwo", method = {RequestMethod.POST, RequestMethod.GET})
    public String testTwo(String[] str) {
        List<String> strings = Arrays.asList(str);
        List<String> replyList = helloStreamingClient.init(strings);
        if (replyList != null && replyList.size() > 0) {
            return JSONObject.toJSONString(replyList);
        }
        return "error";
    }


    /**
     * 服务端流式rpc
     * 一个请求对象，服务端可以传回多个结果对象
     */
    @RequestMapping(value = "/testThree", method = {RequestMethod.POST, RequestMethod.GET})
    public String testThree(String name, String gender) {
        List<Map<String, Object>> resultList = helloOneToManyStreamingClient.init(name, gender);
        if (resultList != null && resultList.size() > 0) {
            return JSONObject.toJSONString(resultList);
        }
        return "error";
    }

    /**
     * 服务端流式rpc
     * 一个请求对象，服务端可以传回多个结果对象
     */
    @RequestMapping(value = "/testFour", method = {RequestMethod.POST, RequestMethod.GET})
    public String testFour(String[] nameList, String[] genderList) {
        List<String> names = Arrays.asList(nameList);
        List<String> genders = Arrays.asList(genderList);
        Map<String, Object> resultMap = helloManyToOneStreamingClient.init(names, genders);
        log.info("result" + resultMap);
        if (resultMap != null) {
            return JSONObject.toJSONString(resultMap);
        }
        return "error";
    }


}
