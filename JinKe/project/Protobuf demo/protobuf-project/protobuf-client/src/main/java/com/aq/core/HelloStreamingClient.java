package com.aq.core;

import com.aq.streaming.HelloStreamingProto;
import com.aq.streaming.StreamingGreeterGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.ClientCallStreamObserver;
import io.grpc.stub.ClientResponseObserver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 双向流式rpc
 * 结合客户端流式rpc和服务端流式rpc，可以传入多个对象，返回多个响应对象
 *
 * @author wu
 */
@Component
@Slf4j
public class HelloStreamingClient {

    private ManagedChannel channel;
    private StreamingGreeterGrpc.StreamingGreeterStub streamingGreeterStub;


    public List<String> init(List<String> strings) {
        this.start();
        return this.createSayHelloStreaming(strings);
    }

    /**
     * 启动服务
     */
    public void start() {
        //链接到server
        channel = ManagedChannelBuilder.forAddress("127.0.0.1", 50051).usePlaintext().build();
        streamingGreeterStub = StreamingGreeterGrpc.newStub(channel);
    }

    /**
     * 停止服务
     */
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public List<String> createSayHelloStreaming(List<String> strings) {
        List<String> resultList = new ArrayList<>();
        final CountDownLatch done = new CountDownLatch(1);
        ClientResponseObserver<HelloStreamingProto.HelloRequest, HelloStreamingProto.HelloReply> clientResponseObserver =
                new ClientResponseObserver<HelloStreamingProto.HelloRequest, HelloStreamingProto.HelloReply>() {

                    ClientCallStreamObserver<HelloStreamingProto.HelloRequest> requestStream;

                    @Override
                    public void beforeStart(final ClientCallStreamObserver<HelloStreamingProto.HelloRequest> requestStream) {
                        this.requestStream = requestStream;
                        //为响应流设置手动流控制。配置响应感觉很落后
                        //流的流控制使用请求流的观察者，但这是它的方式。
                        requestStream.disableAutoInboundFlowControl();

                        //为请求流设置一个感知背压的生产者。将调用onReadyHandler
                        //当消费端有足够的缓冲空间来接收更多的消息时。
                        // 消息被序列化到特定于传输的传输缓冲区中。根据这个缓冲区的大小，
                        //许多消息可能被缓冲，但是它们还没有被发送到服务器。服务器必须调用
                        // request()从客户端获取缓冲消息。
                        requestStream.setOnReadyHandler(new Runnable() {
                            // 使用迭代器可以暂停并继续请求数据的迭代。
                            Iterator<String> iterator = strings.iterator();

                            @Override
                            public void run() {
                                // 从我们在非grpc线程上停止的地方开始生成值。
                                while (requestStream.isReady()) {
                                    if (iterator.hasNext()) {
                                        // 如果要发送更多消息，请发送更多消息。
                                        String name = iterator.next();
                                        HelloStreamingProto.HelloRequest request = HelloStreamingProto.HelloRequest.newBuilder().setName(name).build();
                                        requestStream.onNext(request);
                                    } else {
                                        // 如果没有什么要发送的，就发送完成信号。
                                        requestStream.onCompleted();
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onNext(HelloStreamingProto.HelloReply reply) {
                        log.info("返回消息信息<-- " + reply.getMessage());
                        resultList.add(reply.getMessage());
                        // 给发送者发一个信号，让他发送一条消息
                        requestStream.request(1);
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        done.countDown();
                    }

                    @Override
                    public void onCompleted() {
                        log.info("All Done");
                        done.countDown();
                    }
                };
        try {
            streamingGreeterStub.sayHelloStreaming(clientResponseObserver);
            done.await(1, TimeUnit.MINUTES);
            this.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultList;
    }

}
