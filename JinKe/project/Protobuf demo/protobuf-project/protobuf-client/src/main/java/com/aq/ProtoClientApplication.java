package com.aq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * proto 客户端
 *
 * @author wu
 */
@SpringBootApplication
public class ProtoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProtoClientApplication.class, args);
    }

}
