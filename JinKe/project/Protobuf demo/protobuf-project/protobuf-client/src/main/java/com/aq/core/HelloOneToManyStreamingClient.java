package com.aq.core;

import com.aq.onetomany.HelloOneToManyProto;
import com.aq.onetomany.OneToManyStreamingGreeterGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 服务端流式rpc
 * 一个请求对象，服务端可以传回多个结果对象
 *
 * @author wu
 */
@Component
@Slf4j
public class HelloOneToManyStreamingClient {

    private ManagedChannel channel;
    private OneToManyStreamingGreeterGrpc.OneToManyStreamingGreeterBlockingStub oneToManyStreamingGreeterStub;

    public List<Map<String, Object>> init(String name, String gender) {
        try {
            this.start();
            return this.createSayHelloOneToManyStreaming(name, gender);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 启动服务
     */
    public void start() {
        //链接到server
        channel = ManagedChannelBuilder.forAddress("127.0.0.1", 50052).usePlaintext().build();
        //定义同步阻塞的stub
        oneToManyStreamingGreeterStub = OneToManyStreamingGreeterGrpc.newBlockingStub(channel);
    }

    /**
     * 停止服务
     */
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    /**
     * 发送请求
     */
    public List<Map<String, Object>> createSayHelloOneToManyStreaming(String name, String gender) throws InterruptedException {
        List<Map<String, Object>> resultList = new ArrayList<>();
        HelloOneToManyProto.HelloRequest request = HelloOneToManyProto.HelloRequest.newBuilder().setName(name).setGender(gender).build();
        Iterator<HelloOneToManyProto.HelloReply> replyIterator = oneToManyStreamingGreeterStub.sayHelloOneToManyStreaming(request);

        // 如果服务端使用了onCompleted()方法之后 可以只直接得到相应数据并且返回
//        while (replyIterator.hasNext()) {
//            HelloOneToManyProto.HelloReply reply = replyIterator.next();
//            String message = reply.getMessage();
//            String code = reply.getCode();
//            log.info("response message -->" + message + ",response code -->" + code);
//            Map<String, Object> map = new HashMap<>();
//            map.put("message", message);
//            map.put("code", code);
//            resultList.add(map);
//        }

        //如果服务端   没有  使用了onCompleted之后 可以只直接得到相应数据 但是不会返回 所以创建一个线程  然后睡眠3毫秒之后 再将数据返回出去
        //3毫秒
        new Thread(() -> {
            while (replyIterator.hasNext()) {
                HelloOneToManyProto.HelloReply reply = replyIterator.next();
                String message = reply.getMessage();
                String code = reply.getCode();
                log.info("response message -->" + message + ",response code -->" + code);
                Map<String, Object> map = new HashMap<>();
                map.put("message", message);
                map.put("code", code);
                resultList.add(map);
            }
        }).start();
        Thread.sleep(300);
        //
        //this.shutdown();
        return resultList;
    }
}
