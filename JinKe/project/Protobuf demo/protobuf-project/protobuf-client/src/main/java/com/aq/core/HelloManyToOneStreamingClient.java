package com.aq.core;

import com.aq.mantoone.HelloManyToOneProto;
import com.aq.mantoone.ManyToOneStreamingGreeterGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 客户端流式rpc
 * 客户端传入多个请求对象，服务端返回一个响应结果
 *
 * @author wu
 */
@Component
@Slf4j
public class HelloManyToOneStreamingClient {

    private ManagedChannel channel;
    private ManyToOneStreamingGreeterGrpc.ManyToOneStreamingGreeterStub manyToOneStreamingGreeterStub;

    public Map<String, Object> init(List<String> names, List<String> genders) {
        try {
            this.start();
            return this.createSayHelloManyToOneStreaming(names, genders);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 启动服务
     */
    public void start() {
        //链接到server
        channel = ManagedChannelBuilder.forAddress("127.0.0.1", 50053).usePlaintext().build();
        //定义异步阻塞的stub
        manyToOneStreamingGreeterStub = ManyToOneStreamingGreeterGrpc.newStub(channel);
    }

    /**
     * 停止服务
     */
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    /**
     * 发送请求
     */
    public Map<String, Object> createSayHelloManyToOneStreaming(List<String> names, List<String> genders) throws InterruptedException {
        Map<String, Object> resultMap = new HashMap<>();

        StreamObserver<HelloManyToOneProto.HelloReply> streamObserver = new StreamObserver<HelloManyToOneProto.HelloReply>() {
            @Override
            public void onNext(HelloManyToOneProto.HelloReply helloReply) {
                String message = helloReply.getMessage();
                String code = helloReply.getCode();
                resultMap.put("message", message);
                resultMap.put("code", code);
                log.info("响应结果：" + message + "<-------->" + code);
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onCompleted() {
                channel.shutdown();
            }
        };

        StreamObserver<HelloManyToOneProto.HelloRequest> helloRequestStreamObserver = manyToOneStreamingGreeterStub.sayHelloManyToOneStreaming(streamObserver);
        if (names != null && names.size() > 0 && genders != null && names.size() == genders.size()) {
            for (int i = 0; i < names.size(); i++) {
                HelloManyToOneProto.HelloRequest build = HelloManyToOneProto.HelloRequest.newBuilder().setName(names.get(i)).setGender(genders.get(i)).build();
                helloRequestStreamObserver.onNext(build);
            }
        } else {
            HelloManyToOneProto.HelloRequest build = HelloManyToOneProto.HelloRequest.newBuilder().setName("数据不匹配").setGender("300").build();
            helloRequestStreamObserver.onNext(build);
        }
        helloRequestStreamObserver.onCompleted();
        Thread.sleep(100);
        return resultMap;
    }

}
