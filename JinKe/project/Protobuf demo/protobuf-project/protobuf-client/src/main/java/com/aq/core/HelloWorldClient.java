package com.aq.core;

import com.aq.helloword.GreeterGrpc;
import com.aq.helloword.HelloWorldProto;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * grpc 客户端
 *
 * @author wu
 */
@Component
@Slf4j
public class HelloWorldClient {

    private ManagedChannel channel;
    private GreeterGrpc.GreeterBlockingStub blockingStub;

    public HelloWorldProto.HelloReply init(String str) {
        try {
            this.start();
            HelloWorldProto.HelloReply reply = this.sayHello(str);
            return reply;
        } finally {
            try {
                this.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void start() {
        channel = ManagedChannelBuilder.forAddress("127.0.0.1", 50050).usePlaintext(true).build();
        blockingStub = GreeterGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);

    }

    public HelloWorldProto.HelloReply sayHello(String name) {
        try {
            log.info("Will try to say Hello  " + name + " ...");
            HelloWorldProto.HelloRequest request = HelloWorldProto.HelloRequest.newBuilder().setName(name).build();
            HelloWorldProto.HelloReply response = blockingStub.sayHello(request);
            log.info("result from server: " + response.getMessage());
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            log.info("RPC failed:" + e.getMessage());
            return null;
        }
    }
}
