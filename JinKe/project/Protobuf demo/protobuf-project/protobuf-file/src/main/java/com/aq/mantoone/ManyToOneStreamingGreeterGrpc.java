package com.aq.mantoone;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The greeting service definition.
 * many to one
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.24.0)",
    comments = "Source: hello_many_to_one.proto")
public final class ManyToOneStreamingGreeterGrpc {

  private ManyToOneStreamingGreeterGrpc() {}

  public static final String SERVICE_NAME = "manualflowcontrol.ManyToOneStreamingGreeter";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.aq.mantoone.HelloManyToOneProto.HelloRequest,
      com.aq.mantoone.HelloManyToOneProto.HelloReply> getSayHelloManyToOneStreamingMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SayHelloManyToOneStreaming",
      requestType = com.aq.mantoone.HelloManyToOneProto.HelloRequest.class,
      responseType = com.aq.mantoone.HelloManyToOneProto.HelloReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<com.aq.mantoone.HelloManyToOneProto.HelloRequest,
      com.aq.mantoone.HelloManyToOneProto.HelloReply> getSayHelloManyToOneStreamingMethod() {
    io.grpc.MethodDescriptor<com.aq.mantoone.HelloManyToOneProto.HelloRequest, com.aq.mantoone.HelloManyToOneProto.HelloReply> getSayHelloManyToOneStreamingMethod;
    if ((getSayHelloManyToOneStreamingMethod = ManyToOneStreamingGreeterGrpc.getSayHelloManyToOneStreamingMethod) == null) {
      synchronized (ManyToOneStreamingGreeterGrpc.class) {
        if ((getSayHelloManyToOneStreamingMethod = ManyToOneStreamingGreeterGrpc.getSayHelloManyToOneStreamingMethod) == null) {
          ManyToOneStreamingGreeterGrpc.getSayHelloManyToOneStreamingMethod = getSayHelloManyToOneStreamingMethod =
              io.grpc.MethodDescriptor.<com.aq.mantoone.HelloManyToOneProto.HelloRequest, com.aq.mantoone.HelloManyToOneProto.HelloReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SayHelloManyToOneStreaming"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.aq.mantoone.HelloManyToOneProto.HelloRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.aq.mantoone.HelloManyToOneProto.HelloReply.getDefaultInstance()))
              .setSchemaDescriptor(new ManyToOneStreamingGreeterMethodDescriptorSupplier("SayHelloManyToOneStreaming"))
              .build();
        }
      }
    }
    return getSayHelloManyToOneStreamingMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ManyToOneStreamingGreeterStub newStub(io.grpc.Channel channel) {
    return new ManyToOneStreamingGreeterStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ManyToOneStreamingGreeterBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ManyToOneStreamingGreeterBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ManyToOneStreamingGreeterFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ManyToOneStreamingGreeterFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition.
   * many to one
   * </pre>
   */
  public static abstract class ManyToOneStreamingGreeterImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     *  many to one
     * </pre>
     */
    public io.grpc.stub.StreamObserver<com.aq.mantoone.HelloManyToOneProto.HelloRequest> sayHelloManyToOneStreaming(
        io.grpc.stub.StreamObserver<com.aq.mantoone.HelloManyToOneProto.HelloReply> responseObserver) {
      return asyncUnimplementedStreamingCall(getSayHelloManyToOneStreamingMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSayHelloManyToOneStreamingMethod(),
            asyncClientStreamingCall(
              new MethodHandlers<
                com.aq.mantoone.HelloManyToOneProto.HelloRequest,
                com.aq.mantoone.HelloManyToOneProto.HelloReply>(
                  this, METHODID_SAY_HELLO_MANY_TO_ONE_STREAMING)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * many to one
   * </pre>
   */
  public static final class ManyToOneStreamingGreeterStub extends io.grpc.stub.AbstractStub<ManyToOneStreamingGreeterStub> {
    private ManyToOneStreamingGreeterStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ManyToOneStreamingGreeterStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ManyToOneStreamingGreeterStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ManyToOneStreamingGreeterStub(channel, callOptions);
    }

    /**
     * <pre>
     *  many to one
     * </pre>
     */
    public io.grpc.stub.StreamObserver<com.aq.mantoone.HelloManyToOneProto.HelloRequest> sayHelloManyToOneStreaming(
        io.grpc.stub.StreamObserver<com.aq.mantoone.HelloManyToOneProto.HelloReply> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getSayHelloManyToOneStreamingMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * many to one
   * </pre>
   */
  public static final class ManyToOneStreamingGreeterBlockingStub extends io.grpc.stub.AbstractStub<ManyToOneStreamingGreeterBlockingStub> {
    private ManyToOneStreamingGreeterBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ManyToOneStreamingGreeterBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ManyToOneStreamingGreeterBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ManyToOneStreamingGreeterBlockingStub(channel, callOptions);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * many to one
   * </pre>
   */
  public static final class ManyToOneStreamingGreeterFutureStub extends io.grpc.stub.AbstractStub<ManyToOneStreamingGreeterFutureStub> {
    private ManyToOneStreamingGreeterFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ManyToOneStreamingGreeterFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ManyToOneStreamingGreeterFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ManyToOneStreamingGreeterFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_SAY_HELLO_MANY_TO_ONE_STREAMING = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ManyToOneStreamingGreeterImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ManyToOneStreamingGreeterImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SAY_HELLO_MANY_TO_ONE_STREAMING:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.sayHelloManyToOneStreaming(
              (io.grpc.stub.StreamObserver<com.aq.mantoone.HelloManyToOneProto.HelloReply>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ManyToOneStreamingGreeterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ManyToOneStreamingGreeterBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.aq.mantoone.HelloManyToOneProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ManyToOneStreamingGreeter");
    }
  }

  private static final class ManyToOneStreamingGreeterFileDescriptorSupplier
      extends ManyToOneStreamingGreeterBaseDescriptorSupplier {
    ManyToOneStreamingGreeterFileDescriptorSupplier() {}
  }

  private static final class ManyToOneStreamingGreeterMethodDescriptorSupplier
      extends ManyToOneStreamingGreeterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ManyToOneStreamingGreeterMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ManyToOneStreamingGreeterGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ManyToOneStreamingGreeterFileDescriptorSupplier())
              .addMethod(getSayHelloManyToOneStreamingMethod())
              .build();
        }
      }
    }
    return result;
  }
}
