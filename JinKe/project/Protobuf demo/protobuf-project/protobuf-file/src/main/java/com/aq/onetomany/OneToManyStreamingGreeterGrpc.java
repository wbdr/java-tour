package com.aq.onetomany;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The greeting service definition.
 * one to many
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.24.0)",
    comments = "Source: hello_one_to_many.proto")
public final class OneToManyStreamingGreeterGrpc {

  private OneToManyStreamingGreeterGrpc() {}

  public static final String SERVICE_NAME = "manualflowcontrol.OneToManyStreamingGreeter";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.aq.onetomany.HelloOneToManyProto.HelloRequest,
      com.aq.onetomany.HelloOneToManyProto.HelloReply> getSayHelloOneToManyStreamingMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SayHelloOneToManyStreaming",
      requestType = com.aq.onetomany.HelloOneToManyProto.HelloRequest.class,
      responseType = com.aq.onetomany.HelloOneToManyProto.HelloReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.aq.onetomany.HelloOneToManyProto.HelloRequest,
      com.aq.onetomany.HelloOneToManyProto.HelloReply> getSayHelloOneToManyStreamingMethod() {
    io.grpc.MethodDescriptor<com.aq.onetomany.HelloOneToManyProto.HelloRequest, com.aq.onetomany.HelloOneToManyProto.HelloReply> getSayHelloOneToManyStreamingMethod;
    if ((getSayHelloOneToManyStreamingMethod = OneToManyStreamingGreeterGrpc.getSayHelloOneToManyStreamingMethod) == null) {
      synchronized (OneToManyStreamingGreeterGrpc.class) {
        if ((getSayHelloOneToManyStreamingMethod = OneToManyStreamingGreeterGrpc.getSayHelloOneToManyStreamingMethod) == null) {
          OneToManyStreamingGreeterGrpc.getSayHelloOneToManyStreamingMethod = getSayHelloOneToManyStreamingMethod =
              io.grpc.MethodDescriptor.<com.aq.onetomany.HelloOneToManyProto.HelloRequest, com.aq.onetomany.HelloOneToManyProto.HelloReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SayHelloOneToManyStreaming"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.aq.onetomany.HelloOneToManyProto.HelloRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.aq.onetomany.HelloOneToManyProto.HelloReply.getDefaultInstance()))
              .setSchemaDescriptor(new OneToManyStreamingGreeterMethodDescriptorSupplier("SayHelloOneToManyStreaming"))
              .build();
        }
      }
    }
    return getSayHelloOneToManyStreamingMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static OneToManyStreamingGreeterStub newStub(io.grpc.Channel channel) {
    return new OneToManyStreamingGreeterStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static OneToManyStreamingGreeterBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new OneToManyStreamingGreeterBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static OneToManyStreamingGreeterFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new OneToManyStreamingGreeterFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition.
   * one to many
   * </pre>
   */
  public static abstract class OneToManyStreamingGreeterImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Streams a many greetings
     * </pre>
     */
    public void sayHelloOneToManyStreaming(com.aq.onetomany.HelloOneToManyProto.HelloRequest request,
        io.grpc.stub.StreamObserver<com.aq.onetomany.HelloOneToManyProto.HelloReply> responseObserver) {
      asyncUnimplementedUnaryCall(getSayHelloOneToManyStreamingMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSayHelloOneToManyStreamingMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                com.aq.onetomany.HelloOneToManyProto.HelloRequest,
                com.aq.onetomany.HelloOneToManyProto.HelloReply>(
                  this, METHODID_SAY_HELLO_ONE_TO_MANY_STREAMING)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * one to many
   * </pre>
   */
  public static final class OneToManyStreamingGreeterStub extends io.grpc.stub.AbstractStub<OneToManyStreamingGreeterStub> {
    private OneToManyStreamingGreeterStub(io.grpc.Channel channel) {
      super(channel);
    }

    private OneToManyStreamingGreeterStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OneToManyStreamingGreeterStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new OneToManyStreamingGreeterStub(channel, callOptions);
    }

    /**
     * <pre>
     * Streams a many greetings
     * </pre>
     */
    public void sayHelloOneToManyStreaming(com.aq.onetomany.HelloOneToManyProto.HelloRequest request,
        io.grpc.stub.StreamObserver<com.aq.onetomany.HelloOneToManyProto.HelloReply> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getSayHelloOneToManyStreamingMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * one to many
   * </pre>
   */
  public static final class OneToManyStreamingGreeterBlockingStub extends io.grpc.stub.AbstractStub<OneToManyStreamingGreeterBlockingStub> {
    private OneToManyStreamingGreeterBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private OneToManyStreamingGreeterBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OneToManyStreamingGreeterBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new OneToManyStreamingGreeterBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Streams a many greetings
     * </pre>
     */
    public java.util.Iterator<com.aq.onetomany.HelloOneToManyProto.HelloReply> sayHelloOneToManyStreaming(
        com.aq.onetomany.HelloOneToManyProto.HelloRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getSayHelloOneToManyStreamingMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * one to many
   * </pre>
   */
  public static final class OneToManyStreamingGreeterFutureStub extends io.grpc.stub.AbstractStub<OneToManyStreamingGreeterFutureStub> {
    private OneToManyStreamingGreeterFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private OneToManyStreamingGreeterFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OneToManyStreamingGreeterFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new OneToManyStreamingGreeterFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_SAY_HELLO_ONE_TO_MANY_STREAMING = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final OneToManyStreamingGreeterImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(OneToManyStreamingGreeterImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SAY_HELLO_ONE_TO_MANY_STREAMING:
          serviceImpl.sayHelloOneToManyStreaming((com.aq.onetomany.HelloOneToManyProto.HelloRequest) request,
              (io.grpc.stub.StreamObserver<com.aq.onetomany.HelloOneToManyProto.HelloReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class OneToManyStreamingGreeterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    OneToManyStreamingGreeterBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.aq.onetomany.HelloOneToManyProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("OneToManyStreamingGreeter");
    }
  }

  private static final class OneToManyStreamingGreeterFileDescriptorSupplier
      extends OneToManyStreamingGreeterBaseDescriptorSupplier {
    OneToManyStreamingGreeterFileDescriptorSupplier() {}
  }

  private static final class OneToManyStreamingGreeterMethodDescriptorSupplier
      extends OneToManyStreamingGreeterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    OneToManyStreamingGreeterMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (OneToManyStreamingGreeterGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new OneToManyStreamingGreeterFileDescriptorSupplier())
              .addMethod(getSayHelloOneToManyStreamingMethod())
              .build();
        }
      }
    }
    return result;
  }
}
