package com.aq.config;


import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * nats 配置信息
 *
 * @author wu
 */
@ConfigurationProperties
public class NatsProperties {
    /**
     * 集群nats 服务器配置信息设置（默认优先index为1）
     */
    private String[] natsUrls = {"nats://127.0.0.1:4222", "nats://192.168.1.220:4222"};
    /**
     * 暂未使用
     */
    private String token = null;
    /**
     * 默认的最大重新连接尝试数
     */
    private int maxReconnect = 60;
    /**
     * 尝试重新连接到同一服务器之前的默认等待时间
     */
    private int reconnectWait = 2;
    /**
     * 默认连接超时
     */
    private int connectionTimeout = 2;

    public String[] getNatsUrls() {
        return natsUrls;
    }

    public void setNatsUrls(String[] natsUrls) {
        this.natsUrls = natsUrls;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getMaxReconnect() {
        return maxReconnect;
    }

    public void setMaxReconnect(int maxReconnect) {
        this.maxReconnect = maxReconnect;
    }

    public int getReconnectWait() {
        return reconnectWait;
    }

    public void setReconnectWait(int reconnectWait) {
        this.reconnectWait = reconnectWait;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }
}
