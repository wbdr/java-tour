package com.aq.listener;

import com.aq.annotations.Subscribe;
import io.nats.client.Connection;
import io.nats.client.ConnectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Nats 连接监听
 *
 * @author wu
 */
public class NatsListener implements ConnectionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionListener.class);

    @Override
    public void connectionEvent(Connection conn, Events type) {

        LOGGER.info(String.format("connection status: %s", conn.getStatus()));
        LOGGER.info(String.format("connection url: %s", conn.getConnectedUrl()));

        if (conn.getStatus().equals(Connection.Status.CLOSED)) {
            LOGGER.info("NATS连接关闭，服务器关闭");
            System.exit(1);
        }
    }
}
