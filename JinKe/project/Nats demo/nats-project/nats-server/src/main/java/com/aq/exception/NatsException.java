package com.aq.exception;

/**
 * 注解出问题报错
 *
 * @author wu
 */
public class NatsException extends RuntimeException {

    private String message;

    public NatsException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
