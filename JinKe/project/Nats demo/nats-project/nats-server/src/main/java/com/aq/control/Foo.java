package com.aq.control;

import com.aq.annotations.Subscribe;
import io.nats.client.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 订阅者
 *
 * @author wu
 */
@Component
public class Foo {

    private static final Logger logger = LoggerFactory.getLogger(Foo.class);


    /**
     * 定义订阅者主题
     *
     * @param message
     */
    @Subscribe("haha")
    public void message(Message message) {
        logger.info(message.getSubject() + " : " + new String(message.getData()));
    }

    /**
     * 定义订阅者主题
     *
     * @param message
     */
    @Subscribe("getUser")
    public void getUser(Message message) {
        logger.info(message.getSubject() + " : " + new String(message.getData()));
    }
}