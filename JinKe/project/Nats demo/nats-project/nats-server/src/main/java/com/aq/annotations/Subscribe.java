package com.aq.annotations;


import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 订阅
 *
 * @author wu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface Subscribe {

    /**
     * @return {@link #subscribe()}
     */
    @AliasFor("subscribe")
    String value() default "";

    /**
     * Nats主题
     *
     * @return Nats主题
     */
    @AliasFor("value")
    String subscribe() default "";


    /**
     * 队列名
     *
     * @return nats队列名
     */
    String queue() default "";
}