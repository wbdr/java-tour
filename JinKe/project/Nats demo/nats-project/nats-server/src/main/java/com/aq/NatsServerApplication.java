package com.aq;

import com.aq.annotations.EnableNats;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * nats 订阅者
 *
 * @author wu
 */
@EnableNats
@SpringBootApplication
public class NatsServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(NatsServerApplication.class, args);
    }
}
