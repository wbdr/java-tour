package com.aq.control;

import com.aq.annotations.Subscribe;
import io.nats.client.Connection;
import io.nats.client.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订阅者
 *
 * @author wu
 */
@Component
public class FooResponse {

    private static final Logger logger = LoggerFactory.getLogger(FooResponse.class);

    @Autowired
    private Connection connection;

    /**
     * 定义订阅者主题
     *
     * @param message
     */
    @Subscribe("getUserReponse")
    public void getUser(Message message) {
        logger.info(message.getSubject() + " : " + new String(message.getData()));
        //响应请求体
        connection.publish(message.getReplyTo(), "response 结果信息".getBytes());
    }
}