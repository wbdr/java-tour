package com.aq.config;

import com.aq.listener.NatsListener;
import io.nats.client.Connection;
import io.nats.client.Nats;
import io.nats.client.Options;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.time.Duration;

/**
 * Nats配置类
 *
 * @author wu
 */
@Configuration
@EnableConfigurationProperties(NatsProperties.class)
public class NatsConfiguration {

    /**
     * nats配置信息注入bean中（Connection）
     *
     * @param properties 资源信息文件
     */
    @Bean
    public Connection natsConnection(NatsProperties properties) throws IOException, InterruptedException {
        Options.Builder builder = new Options.Builder()
                .servers(properties.getNatsUrls())
                .token(properties.getToken())
                .connectionListener(new NatsListener())
                .maxReconnects(properties.getMaxReconnect())
                .reconnectWait(Duration.ofSeconds(properties.getReconnectWait()))
                .connectionTimeout(Duration.ofSeconds(properties.getConnectionTimeout()));
        return Nats.connect(builder.build());
    }

}
