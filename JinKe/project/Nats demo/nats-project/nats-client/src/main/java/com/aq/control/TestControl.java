package com.aq.control;

import io.nats.client.Connection;
import io.nats.client.Message;
import io.nats.client.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * 测试控制层
 *
 * @author wu
 */
@RestController
@RequestMapping("/test")
public class TestControl {

    private static final Logger logger = LoggerFactory.getLogger(TestControl.class);

    @Autowired
    private Connection connection;

    /**
     * nats发布信息（sub/pub）发布/订阅
     *
     * @param string       发布主题
     * @param subscribeKey 发布信息
     */
    @RequestMapping(value = "/get", method = {RequestMethod.POST, RequestMethod.GET})
    public void test(String string, String subscribeKey) {

        try {
            connection.publish(subscribeKey, string.getBytes());
            connection.flush(Duration.ZERO);
            logger.info("消息发布成功！");
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * nats发布信息（request/response）请求响应
     *
     * @param string       发布主题
     * @param subscribeKey 发布信息
     */
    @RequestMapping(value = "/getTwo", method = {RequestMethod.POST, RequestMethod.GET})
    public void testTwo(String string, String subscribeKey) {

        try {
            CompletableFuture<Message> getUser = connection.request(subscribeKey, string.getBytes());
            Message message = getUser.get();
            logger.info(new String(message.getData()));
            connection.flush(Duration.ZERO);
            logger.info("消息发布成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
