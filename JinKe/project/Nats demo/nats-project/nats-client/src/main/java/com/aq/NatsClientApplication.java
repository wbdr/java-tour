package com.aq;

import com.aq.annotations.EnableNats;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * nats发布者
 *
 * @author wu
 */
@EnableNats
@SpringBootApplication
public class NatsClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(NatsClientApplication.class, args);
    }
}
