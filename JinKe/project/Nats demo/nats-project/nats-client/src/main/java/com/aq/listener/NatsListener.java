package com.aq.listener;

import io.nats.client.Connection;
import io.nats.client.ConnectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Nats 连接监听
 *
 * @author wu
 */
public class NatsListener implements ConnectionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionListener.class);

    @Override
    public void connectionEvent(Connection conn, Events type) {
        LOGGER.info(String.format("connection url: %s", conn.getConnectedUrl(), "connection status: %s", conn.getStatus()));
        if (conn.getStatus().equals(Connection.Status.CLOSED)) {
            LOGGER.info("NATS连接关闭，服务器关闭");
            //当nats服务断掉之后，重新连接60次后还没连上，关闭该服务进程
            System.exit(1);
        }
    }
}
