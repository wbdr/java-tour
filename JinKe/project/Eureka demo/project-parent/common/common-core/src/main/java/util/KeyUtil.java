package util;

import java.util.UUID;

/**
 * 生成数据信息工具类
 *
 * @author wu
 */
public class KeyUtil {
    public static String genKey() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
