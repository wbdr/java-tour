/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 abel533@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.aq.base;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 基础实体
 *
 * @Author wu
 */
public class BaseEntity implements Serializable {
    private static final int MAX_ROWS = 50;

    @JsonIgnore
    @Transient
    private Integer page = 1;

    @JsonIgnore
    @Transient
    private Integer rows = 20;

    public Integer getPage() {
        this.page = this.page == null ? 1 : this.page;
        return this.page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setPage(String page) {
        try {
            Integer tempPage = Integer.valueOf(page);
            if (tempPage > 0) {
                this.page = tempPage;
            } else {
                this.page = 1;
            }
        } catch (Exception e) {
            this.page = 1;
        }
    }

    public Integer getRows() {
        this.rows = this.rows == null ? 15 : this.rows;
        return this.rows > MAX_ROWS ? MAX_ROWS : this.rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public void setRows(String rows) {
        try {
            Integer tempRows = Integer.valueOf(rows);
            this.rows = tempRows > MAX_ROWS ? MAX_ROWS : tempRows;
        } catch (Exception e) {
            this.rows = 20;
        }
    }
}
