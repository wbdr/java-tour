package com.aq.base;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import util.KeyUtil;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 基础业务类
 *
 * @Author wu
 */
public abstract class BaseBusiness<T extends BaseEntity> implements IBaseBusiness<T> {

    @Autowired
    protected MyMapper<T> myMapper; // 由private改为protected，让子类也可以直接使用相关方法，这里要使用spring的类型注入，传入的T必须为实体对象，不能是继承实体对象的Bean

//    public abstract MyMapper<T> myMapper;

    /**
     * 分页方法（实体）该处定义方法供子类实现
     *
     * @param t
     * @return
     */
    @Override
    public PageBean<T> getByPage(T t) {
        return null;
    }

    /**
     * 添加
     *
     * @param t
     * @return
     */
    @Override
    @Transactional(readOnly = false)
    public int add(T t) {
        int res = myMapper.insertSelective(t);
        return res;
    }

    /**
     * 根据id获得
     *
     * @param id
     * @return
     */
    @Override
    public T get(Object id) {
        return myMapper.selectByPrimaryKey(id);
    }

    /**
     * 根据对象条件获得
     *
     * @param t
     * @return
     */
    @Override
    public List<T> get(T t) {
        return myMapper.select(t);
    }

    /**
     * 物理删除
     *
     * @param id
     * @return
     */
    @Override
    public int del(Object id) {
        return myMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更改数据
     *
     * @param t
     * @return
     */
    @Override
    public int modify(T t) {
        return myMapper.updateByPrimaryKeySelective(t);
    }

    /**
     * 查询全部
     *
     * @return
     */
    @Override
    public List<T> getAll() {
        return myMapper.selectAll();
    }

    /**
     * 批量删除<需要自己手写sql>（逻辑删除）
     *
     * @param ids
     * @return
     */
    @Override
    public int removeByIdsSql(String ids) {
        if (null != ids) {
            String[] arr = ids.split(",");
            if (null != arr && arr.length > 0) {
                return myMapper.updateByRemoveIds(arr);
            }
        }
        return 0;
    }

    /**
     * 批量删除（逻辑删除）
     *
     * @param valT 要更改的值
     * @param ids  更改ids
     * @return
     */
    @Override
    public int removeByIds(T valT, String ids) {
        return updateByIds(valT, ids);
    }

    /**
     * 保存或者更新，id不自增的情况<该方法比较旧，有些新增属性没有赋值>（如有别的需要手动增加的属性，则重写该类后，再次调用该类，重写注意事务readonly）
     *
     * @param t
     * @param otherPara
     * @return
     */
    @Override
    public int saveIdNoAuto(T t, Map<String, Object> otherPara) {
        try {
            Field[] fs = t.getClass().getDeclaredFields(); //得到类中的所有属性集合
            Field f;
            boolean isNew = true; // 是否为新纪录
            String keyProName = null; // 主键名称
            Method getM;
            Method setM;
            for (int i = 0; i < fs.length; i++) {
                f = fs[i];
                if (null != f) {
                    f.setAccessible(true); //设置些属性是可以访问的
                    if (null != f.getAnnotation(Id.class)) {
                        keyProName = f.getName();
                        keyProName = keyProName.substring(0, 1).toUpperCase() + keyProName.substring(1); // 将属性的首字符大写，方便构造get，set方法
                        getM = t.getClass().getMethod("get" + keyProName);
                        Object val = getM.invoke(t);
                        if (null != val && StringUtils.isNotBlank(String.valueOf(val))) { // 执行get方法，如果获取到的id不为空，证明是修改操作
                            isNew = false;
                        }
                        break;
                    }
                }
            }
            if (isNew) {
                // 设置主键
                getM = t.getClass().getMethod("get" + keyProName);
                setM = t.getClass().getMethod("set" + keyProName, getM.getReturnType());
                setM.invoke(t, KeyUtil.genKey());
                return myMapper.insertSelective(t);
            } else {
                return myMapper.updateByPrimaryKeySelective(t);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
        }
        return 0;
    }

    /**
     * 保存或者更新，id自增<涵盖了新增属性的赋值>（如有别的需要手动增加的属性，则重写该类后，再次调用该类，重写注意事务readonly）
     *
     * @param t
     * @return
     */
    @Override
    public int saveIdAuto(T t, Map<String, Object> otherPara) {
        try {
            Field[] fs = t.getClass().getDeclaredFields(); //得到类中的所有属性集合
            Field f;
            boolean isNew = true; // 是否为新纪录
            String keyProName = null; // 主键名称
            Method getM;
            Method setM;
            for (int i = 0; i < fs.length; i++) {
                f = fs[i];
                if (null != f) {
                    f.setAccessible(true); //设置些属性是可以访问的
                    if (null != f.getAnnotation(Id.class)) {
                        keyProName = f.getName();
                        keyProName = keyProName.substring(0, 1).toUpperCase() + keyProName.substring(1); // 将属性的首字符大写，方便构造get，set方法
                        getM = t.getClass().getMethod("get" + keyProName);
                        Object val = getM.invoke(t);
                        if (null != val && StringUtils.isNotBlank(String.valueOf(val))) { // 执行get方法，如果获取到的id不为空，证明是修改操作
                            isNew = false;
                        }
                        break;
                    }
                }
            }
            if (isNew) {
                // 设置其他属性（暂定为固定字段固定类型）
//                // createDate
//                getM = t.getClass().getMethod("get" + "CreateTime");
//                setM = t.getClass().getMethod("set" + "CreateTime", getM.getReturnType());
//                setM.invoke(t, new Date());
//                // createBy
//                getM = t.getClass().getMethod("get" + "CreateBy");
//                setM = t.getClass().getMethod("set" + "CreateBy", getM.getReturnType());
//                setM.invoke(t, otherPara.get("loginName"));
//                // delFlag
//                getM = t.getClass().getMethod("get" + "DelFlag");
//                setM = t.getClass().getMethod("set" + "DelFlag", getM.getReturnType());
//                setM.invoke(t, Byte.valueOf("0"));
                return myMapper.insertSelective(t);
            } else {
                // 设置其他属性（暂定为固定字段固定类型）
                // updateDate
//                getM = t.getClass().getMethod("get" + "UpdateTime");
//                setM = t.getClass().getMethod("set" + "UpdateTime", getM.getReturnType());
//                setM.invoke(t, new Date());
//                // updateBy
//                getM = t.getClass().getMethod("get" + "UpdateBy");
//                setM = t.getClass().getMethod("set" + "UpdateBy", getM.getReturnType());
//                setM.invoke(t, otherPara.get("loginName"));
                return myMapper.updateByPrimaryKeySelective(t);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
        }
        return 0;
    }

    /**
     * 批量更新
     *
     * @param valT 要更改的值
     * @param ids  更改ids
     * @return
     */
    @Override
    public int updateByIds(T valT, String ids) {
        int res = 0;
        if (null != ids) {
            String[] arr = ids.split(",");
            if (null != arr && arr.length > 0) {
                Example e = new Example(valT.getClass());
                Example.Criteria criteria = e.createCriteria();
                criteria.andIn("id", Arrays.asList(arr));
                res = myMapper.updateByExampleSelective(valT, e);
            }
        }
        return res;
    }
}
