package com.aq.business.impl;

import com.aq.base.BaseBusiness;
import com.aq.basic.entity.ManagerInfoEntity;
import com.aq.basic.mapper.ManagerInfoEntityMapper;
import com.aq.business.IManagerInfoBusiness;
import com.aq.dao.IManagerInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * service impl
 *
 * @Author wu
 */
@Service
@Transactional(readOnly = true)
public class ManagerInfoBusiness extends BaseBusiness<ManagerInfoEntity> implements IManagerInfoBusiness {

    @Autowired
    private IManagerInfoDao managerInfoDao;
    @Autowired
    private ManagerInfoEntityMapper managerInfoEntityMapper;


}
