package com.aq.base;

import com.github.pagehelper.Page;

import java.io.Serializable;
import java.util.List;

/**
 * 分页数据对象
 *
 * @Author wu
 */
public class PageBean<T> implements Serializable {
    private static final long serialVersionUID = 8656597559014685635L;

    private static final int MAX_SHOW_PAGE = 10; // 分页组件最大显示数

    private long total;        //总记录数
    private List<T> list;    //结果集
    private int pageNum;    // 第几页
    private int pageSize;    // 每页记录数
    private int pages;        // 总页数
    private int size;        // 当前页的数量 <= pageSize，该属性来自ArrayList的size属性

    //是否为第一页
    private boolean isFirstPage = false;
    //是否为最后一页
    private boolean isLastPage = false;
    //是否有前一页
    private boolean hasPreviousPage = false;
    //是否有下一页
    private boolean hasNextPage = false;

    // 是否需要返回html代码
    private boolean needHtml = true;
    // 分页html
    private String html;

    /**
     * 包装Page对象，因为直接返回Page对象，在JSON处理以及其他情况下会被当成List来处理，
     * 而出现一些问题。
     *
     * @param list page结果
     */
    public PageBean(List<T> list, boolean needHtml) {
        if (list instanceof Page) {
            Page<T> page = (Page<T>) list;
            this.pageNum = page.getPageNum();
            this.pageSize = page.getPageSize();
            this.total = page.getTotal();
            this.pages = page.getPages();
            this.list = page;

//            if(page != null && page.size()<0){
//                this.list = page;
//            }
            this.size = page.size();

            if (pageNum == 1) {
                this.isFirstPage = true;
            }
            if (pageNum == pages) {
                this.isLastPage = true;
            }
            if (pageNum > 1) {
                this.hasPreviousPage = true;
            }
            if (pageNum < pages) {
                this.hasNextPage = true;
            }
            this.needHtml = needHtml;
            html();
        }
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public boolean isFirstPage() {
        return isFirstPage;
    }

    public void setFirstPage(boolean firstPage) {
        isFirstPage = firstPage;
    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public void setLastPage(boolean lastPage) {
        isLastPage = lastPage;
    }

    public boolean isHasPreviousPage() {
        return hasPreviousPage;
    }

    public void setHasPreviousPage(boolean hasPreviousPage) {
        this.hasPreviousPage = hasPreviousPage;
    }

    public boolean isHasNextPage() {
        return hasNextPage;
    }

    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    private void html() {
        if (needHtml) {
            StringBuilder sb = new StringBuilder();
            if (pageNum > 1) {
                sb.append("<li class='prev'>");
                sb.append("<a href='javascript:;' onclick='doPage(" + 1 + ")'>首页</a>");
            } else {
                sb.append("<li class='prev disabled'>");
                sb.append("<span>首页</span>");
            }

            if (this.hasPreviousPage) {
                sb.append("<li class='prev'>");
                sb.append("<a href='javascript:;' onclick='doPage(" + (pageNum - 1) + ")'>上一页</a>");
            } else {
                sb.append("<li class='prev disabled'>");
                sb.append("<span>上一页</span>");
            }

            // 方式一：每次点击的页在第一个，范围不够除外
            int off = MAX_SHOW_PAGE - 1;
            int endPage = pageNum + off;// 总共显示10个页码
            int i = 1;
            int max = 1;
            if (endPage < pages) {
                i = pageNum;
                max = endPage;
            } else {
                i = pages - off;
                i = i < 1 ? 1 : i;
                max = pages;
            }
            for (; i <= max; i++) {
                if (this.pageNum == i) {
                    sb.append("<li class='active'><a href='javascript:;'>");
                } else {
                    sb.append("<li><a href='javascript:;' onclick='doPage(" + i + ")'>");
                }
                sb.append(i);
                sb.append("</a></li>");
            }

            if (this.hasNextPage) {
                sb.append("<li class='next'>");
                sb.append("<a href='javascript:;' onclick='doPage(" + (pageNum + 1) + ")'>下一页</a>");
            } else {
                sb.append("<li class='next disabled'>");
                sb.append("<span>下一页</span>");
            }

            if (pageNum < pages) {
                sb.append("<li class='prev'>");
                sb.append("<a href='javascript:;' onclick='doPage(" + pages + ")'>尾页</a>");
            } else {
                sb.append("<li class='prev disabled'>");
                sb.append("<span>尾页</span>");
            }
            this.html = sb.toString();
        }
    }

    public boolean isNeedHtml() {
        return needHtml;
    }

    public void setNeedHtml(boolean needHtml) {
        this.needHtml = needHtml;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}