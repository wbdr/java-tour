package com.aq.base;


import java.util.List;
import java.util.Map;

/**
 * 基础业务接口
 *
 * @Author wu
 */
public interface IBaseBusiness<T extends BaseEntity> {

    /**
     * 分页方法（实体）该处定义方法供子类实现
     *
     * @param t
     * @return
     */
    PageBean<T> getByPage(T t);


    /**
     * 添加
     *
     * @param t
     * @return
     */
    int add(T t);

    /**
     * 根据id获得
     *
     * @param id
     * @return
     */
    T get(Object id);

    /**
     * 根据对象条件获得
     *
     * @param t
     * @return
     */
    List<T> get(T t);

    /**
     * 物理删除
     *
     * @param id
     * @return
     */
    int del(Object id);

    /**
     * 更改数据
     *
     * @param t
     * @return
     */
    int modify(T t);

    /**
     * 查询全部
     *
     * @return
     */
    List<T> getAll();

    /**
     * 批量删除<需要自己手写sql>（逻辑删除）
     *
     * @param ids
     * @return
     */
    int removeByIdsSql(String ids);

    /**
     * 批量删除（逻辑删除）
     *
     * @param valT 要更改的值
     * @param ids  更改ids
     * @return
     */
    int removeByIds(T valT, String ids);

    /**
     * 保存或者更新，id不自增的情况<该方法比较旧，有些新增属性没有赋值>（如有别的需要手动增加的属性，则重写该类后，再次调用该类，重写注意事务readonly）
     *
     * @param t
     * @param otherPara
     * @return
     */
    int saveIdNoAuto(T t, Map<String, Object> otherPara);

    /**
     * 保存或者更新，id自增<涵盖了新增属性的赋值>（如有别的需要手动增加的属性，则重写该类后，再次调用该类，重写注意事务readonly）
     *
     * @param t
     * @return
     */
    int saveIdAuto(T t, Map<String, Object> otherPara);

    /**
     * 批量更新
     *
     * @param valT 要更改的值
     * @param ids  更改ids
     * @return
     */
    int updateByIds(T valT, String ids);

}
