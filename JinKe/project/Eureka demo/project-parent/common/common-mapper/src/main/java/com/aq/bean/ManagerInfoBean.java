package com.aq.bean;

import com.aq.basic.entity.ManagerInfoEntity;
import lombok.Data;

/**
 * managerBean
 * 增加字段的时候直接往这里面进行增加/减少
 *
 * @author wu
 */
@Data
public class ManagerInfoBean extends ManagerInfoEntity {


}
