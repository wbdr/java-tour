package com.aq.basic.mapper;

import com.aq.base.MyMapper;
import com.aq.basic.entity.ManagerInfoEntity;

/**
 * 继承自定义的mapper类
 *
 * @author wu
 */
public interface ManagerInfoEntityMapper extends MyMapper<ManagerInfoEntity> {
}