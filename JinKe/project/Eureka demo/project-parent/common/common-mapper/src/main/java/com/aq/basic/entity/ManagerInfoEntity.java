package com.aq.basic.entity;

import com.aq.base.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Table(name = "sys_manager_info")
public class ManagerInfoEntity extends BaseEntity {
    /**
     * 用户表
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    /**
     * 姓名
     */
    @Column(name = "manager_name")
    private String managerName;

    /**
     * 管理员电话号码
     */
    @Column(name = "manager_phone")
    private String managerPhone;

    /**
     * 管理员密码
     */
    @Column(name = "manager_pwd")
    private String managerPwd;

    /**
     * 管理员角色：100001超级管理员，100002普通管理员
     */
    @Column(name = "manager_role")
    private String managerRole;

    /**
     * 注册时间/添加时间
     */
    @Column(name = "register_date_time")
    private Date registerDateTime;

    /**
     * 获取用户表
     *
     * @return id - 用户表
     */
    public String getId() {
        return id;
    }

    /**
     * 设置用户表
     *
     * @param id 用户表
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取姓名
     *
     * @return manager_name - 姓名
     */
    public String getManagerName() {
        return managerName;
    }

    /**
     * 设置姓名
     *
     * @param managerName 姓名
     */
    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    /**
     * 获取管理员电话号码
     *
     * @return manager_phone - 管理员电话号码
     */
    public String getManagerPhone() {
        return managerPhone;
    }

    /**
     * 设置管理员电话号码
     *
     * @param managerPhone 管理员电话号码
     */
    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    /**
     * 获取管理员密码
     *
     * @return manager_pwd - 管理员密码
     */
    public String getManagerPwd() {
        return managerPwd;
    }

    /**
     * 设置管理员密码
     *
     * @param managerPwd 管理员密码
     */
    public void setManagerPwd(String managerPwd) {
        this.managerPwd = managerPwd;
    }

    /**
     * 获取管理员角色：100001超级管理员，100002普通管理员
     *
     * @return manager_role - 管理员角色：100001超级管理员，100002普通管理员
     */
    public String getManagerRole() {
        return managerRole;
    }

    /**
     * 设置管理员角色：100001超级管理员，100002普通管理员
     *
     * @param managerRole 管理员角色：100001超级管理员，100002普通管理员
     */
    public void setManagerRole(String managerRole) {
        this.managerRole = managerRole;
    }

    /**
     * 获取注册时间/添加时间
     *
     * @return register_date_time - 注册时间/添加时间
     */
    public Date getRegisterDateTime() {
        return registerDateTime;
    }

    /**
     * 设置注册时间/添加时间
     *
     * @param registerDateTime 注册时间/添加时间
     */
    public void setRegisterDateTime(Date registerDateTime) {
        this.registerDateTime = registerDateTime;
    }
}