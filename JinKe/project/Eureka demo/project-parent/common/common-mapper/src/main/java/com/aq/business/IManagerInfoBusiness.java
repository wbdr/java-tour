package com.aq.business;


import com.aq.base.IBaseBusiness;
import com.aq.basic.entity.ManagerInfoEntity;

/**
 * service
 *
 * @Author wu
 */
public interface IManagerInfoBusiness extends IBaseBusiness<ManagerInfoEntity> {

}
