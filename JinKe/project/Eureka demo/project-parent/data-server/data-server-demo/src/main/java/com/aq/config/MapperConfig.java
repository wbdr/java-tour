package com.aq.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * 通用mapper配置类
 *
 * @Author wu
 */
@Configuration
@MapperScan(basePackages = {"com.aq.dao", "com.aq.basic.mapper", "com.aq.business"})
public class MapperConfig {
}