package com.aq.controller;

import com.aq.basic.entity.ManagerInfoEntity;
import com.aq.business.IManagerInfoBusiness;
import com.aq.entity.Demo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("demo")
@Slf4j
public class DataServerDemoController {
    @Autowired
    private Environment environment;

    @Autowired
    private IManagerInfoBusiness managerInfoBusiness;

    @GetMapping("list")
    public List<Demo> listDemo() {
        List<Demo> list = new ArrayList<>();
        for (int i = 0; i < 150; i++) {
            Demo demo = new Demo();
            demo.setName("name:" + i);
            demo.setDesc("desc:" + i);
            demo.setNum(i);
            list.add(demo);
        }
        String port = environment.getProperty("server.port");
        log.info("当前端口号：" + port);
        List<ManagerInfoEntity> all = managerInfoBusiness.getAll();
        log.info(">>>>>>>>>>>>>>>>>>>" + all.size());
        return list;
    }
}
