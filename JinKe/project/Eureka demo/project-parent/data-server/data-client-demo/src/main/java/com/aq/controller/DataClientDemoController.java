package com.aq.controller;

import com.aq.entity.Demo;
import com.aq.service.DataServerDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("client")
public class DataClientDemoController {
    @Autowired
    private DataServerDemoService dataServerDemoService;

    @GetMapping("list")
    public List<Demo> listDemo() {
        return dataServerDemoService.listDemo();
    }
}
