package com.aq.entity;

import lombok.Data;


import java.io.Serializable;
@Data
public class Demo implements Serializable {
    private String name;
    private Integer num;
    private String desc;
}
