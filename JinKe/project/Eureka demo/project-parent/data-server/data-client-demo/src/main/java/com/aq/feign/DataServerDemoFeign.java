package com.aq.feign;

import com.aq.entity.Demo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "${data.server.demo}", path = "demo")
public interface DataServerDemoFeign {
    @GetMapping("list")
    public List<Demo> listDemo();
}
