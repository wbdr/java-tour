package com.aq.service.impl;

import com.aq.entity.Demo;
import com.aq.feign.DataServerDemoFeign;
import com.aq.service.DataServerDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataServerDemoServiceImpl implements DataServerDemoService {
    @Autowired
    private DataServerDemoFeign dataServerDemoFeign;

    @Override
    public List<Demo> listDemo() {
        return dataServerDemoFeign.listDemo();
    }
}
