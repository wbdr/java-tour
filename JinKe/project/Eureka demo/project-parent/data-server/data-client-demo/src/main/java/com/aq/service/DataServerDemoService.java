package com.aq.service;

import com.aq.entity.Demo;

import java.util.List;


public interface DataServerDemoService {
    List<Demo> listDemo();
}
