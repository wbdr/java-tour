import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/home'
import my from '@/views/my'

Vue.use(Router)

export default new Router({
	routes: [{
			path: '/',
			/**父容器*/
			component: Home,
			name: '',
			iconCls: 'fa fa-home',
			leaf: true, //只有一个节点
			children: [{
				path: '/my',
				component: my,
				name: '首页'
			}]
		},
		{
			path: '/my',
			name: 'my',
			component: Home
		}
	]
})