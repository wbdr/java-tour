一个开源项目cpdetector，实现更复杂的文件编码检测
说明：该工具类可以针对一些文件根据需求修改他的文件编码格式类型；比如txt、csv、xlsx类型的文件要转换为GBK或者UTF-8编码格式 ，可以使用该工具类进行操作。
详情看：https://www.cnblogs.com/king1302217/p/4003060.html