package com.cangoonline.util;

import info.monitorenter.cpdetector.io.ASCIIDetector;
import info.monitorenter.cpdetector.io.CodepageDetectorProxy;
import info.monitorenter.cpdetector.io.JChardetFacade;
import info.monitorenter.cpdetector.io.ParsingDetector;
import info.monitorenter.cpdetector.io.UnicodeDetector;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 使用一个开源项目cpdetector，实现更复杂的文件编码检测
 * 详情看：https://www.cnblogs.com/king1302217/p/4003060.html
 * @author kancy
 * 
 */
public class FileEncodeDetector {
	//获取文件的编码格式
	public static String getFileEncode(File file) {
		CodepageDetectorProxy detector = CodepageDetectorProxy.getInstance();
		detector.add(new ParsingDetector(false));
		detector.add(JChardetFacade.getInstance());
		detector.add(ASCIIDetector.getInstance());
		detector.add(UnicodeDetector.getInstance());
		java.nio.charset.Charset charset = null;
		String encoding = null;
		try {
			charset = detector.detectCodepage(file.toURI().toURL());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (charset != null) {
			encoding = charset.name();
			if ("windows-1252".equalsIgnoreCase(encoding)) {
				encoding = "unicode";
			}
			return encoding;
		} else {
			return null;
		}
	}

	public static String getFileEncode(String pathname) {
		return getFileEncode(new File(pathname));
	}
/*
	public static void main(String[] args) throws IOException, FileNotFoundException {
		String path = "C:\\uploadFiles\\output\\output.csv";
		System.out.println(getFileEncode(path));
		String curInputCsvPath = "C:\\uploadFiles\\output\\output.csv"; //Const.MYFILEPATH + Const.MYINPUTFILEPATH + "input.csv";//input输入文件
		try {
			FileCharsetConverter.convert(curInputCsvPath, "UTF-8", "GBK");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/
}
